package ua.oksana.sendev.tictactoebeta.logic;

import ua.oksana.sendev.tictactoebeta.constants.Constants;
import ua.oksana.sendev.tictactoebeta.intermediaries.IMoveListener;
import ua.oksana.sendev.tictactoebeta.participants.Player;
import ua.oksana.sendev.tictactoebeta.parts.Cell;
import ua.oksana.sendev.tictactoebeta.parts.Field;
import ua.oksana.sendev.tictactoebeta.parts.MiniField;
import ua.oksana.sendev.tictactoebeta.parts.ParentArea;

/**
 * Created by Oksana on 08.12.2014.
 */
public class Game implements IGameController {
    private IMoveListener moveListener;
    private int moveCounter = 0;
    private Field field;
    private byte miniFieldId, cellId;
    private MiniField currentMiniField;
    private Player currentPlayer;
    private byte permittedMiniField = Constants.PERMITTED_ALL;
    private boolean isSuccessfulMove, isGameWinner, isGameOver, checkForBothPlayers, winnerIsNotCurrentPlayer;
    private char gameOverType;
    private byte[] winningLinePoints;
    private MoveSaver g;


    public Game(IMoveListener moveListener) {
        field = new Field();
        g = new MoveSaver();
        this.moveListener = moveListener;
    }

    @Override
    public boolean isUndo() {
        return moveCounter != 0;
    }

    @Override
    public boolean set(Player player, byte miniFieldId, byte cellId) {
        boolean isSuccessfulSet = false;
        if (!isGameWinner) {
            this.currentPlayer = player;
            this.miniFieldId = miniFieldId;
            this.cellId = cellId;
            isSuccessfulSet = true;
        }
        return isSuccessfulSet;
    }

    @Override
    public boolean undoLastMove() {
        boolean isUndo = false;
        if (moveCounter > 0) {
            if (isGameOver) {
                isGameOver = false;
                isGameWinner = false;
                if (winnerIsNotCurrentPlayer) {
                    winnerIsNotCurrentPlayer = false;
                    moveListener.changePlayer();
                }
            }
            MiniField lastMF = (MiniField) field.getTarget(g.getSavedMiniFieldId(moveCounter));
            if (lastMF.isOccupied()) {
                lastMF.unOccupy();
                field.refresh(lastMF);
            }
            Cell lastC = (Cell) lastMF.getTarget(g.getSavedCellId(moveCounter));
            lastC.unOccupy();
            lastMF.refresh(lastC);
            permittedMiniField = g.getSavedPermittedMiniField(moveCounter);
            moveListener.markField(field.getMarkMovesInstruction(), field.getMarkMarkersInstruction(permittedMiniField), permittedMiniField);
            g.cleanUp(moveCounter);
            moveCounter--;
            isUndo = true;
        }
        return isUndo;

    }


    private boolean checkIfMiniFieldForNextMoveIsOccupied(byte cellId) {
        return field.getTarget(cellId).isOccupied();
    }

    @Override
    public boolean start() {
        isSuccessfulMove = false;
        changeMiniFieldState();
        if (isSuccessfulMove) {
            moveCounter++;
            checkForWinner();
            g.save(moveCounter, permittedMiniField, miniFieldId, cellId);
            markFieldForNextMove(cellId);
        }
        if (isGameOver) {
            moveListener.overTheGame(gameOverType, winningLinePoints, winnerIsNotCurrentPlayer);
        }
        return isSuccessfulMove;

    }

    private void markFieldForNextMove(byte cellId) {
        boolean miniFieldForNextMoveIsOccupied = checkIfMiniFieldForNextMoveIsOccupied(cellId);
        if (miniFieldForNextMoveIsOccupied) {
            permittedMiniField = Constants.PERMITTED_ALL;
        } else {
            permittedMiniField = cellId;
        }
        moveListener.markField(field.getMarkMovesInstruction(), field.getMarkMarkersInstruction(permittedMiniField), permittedMiniField);
    }


    private void changeMiniFieldState() {
        currentMiniField = (MiniField) field.getTarget(miniFieldId);
        if (!currentMiniField.isOccupied()) {
            Cell currentCell = (Cell) currentMiniField.getTarget(cellId);
            if (!currentCell.isOccupied()) {
                currentCell.setValue(currentPlayer.getValue());
                currentCell.occupy();
                currentMiniField.refresh(currentCell);
                isSuccessfulMove = true;
            }
        }
    }


    private void checkForWinner() {
        checkForMiniFieldWinner();
        checkForGameWinner();

    }

    private void checkForMiniFieldWinner() {
        boolean isMiniFieldWinner = Checker.checkForWinner(currentMiniField.getCurrentState(), currentPlayer.getValue());
        if (isMiniFieldWinner) {
            currentMiniField.setValue(currentPlayer.getValue());
            currentMiniField.occupy();
        } else {
            if (Checker.checkForDraw(currentMiniField)) {
                checkForBothPlayers = true;
            }
        }
        field.refresh(currentMiniField);

    }

    private void checkForGameWinner() {
        isGameWinner = Checker.checkForWinner(field.getCurrentState(), currentPlayer.getValue());
        if (!isGameWinner & checkForBothPlayers) {
            byte value;
            if (currentPlayer.getName().equals(Constants.CROSS_NAME)) {
                value = Constants.ZERO_VALUE;
            } else {
                value = Constants.CROSS_VALUE;
            }
            isGameWinner = Checker.checkForWinner(field.getCurrentState(), value);
            checkForBothPlayers = false;
            if (isGameWinner) {
                winnerIsNotCurrentPlayer = true;
            }
        }
        if (isGameWinner) {
            gameOverType = Constants.WIN;
            winningLinePoints = Checker.getWinningLinePoints();
            isGameOver = true;
        } else {
            boolean isGameDraw = Checker.checkForDraw(field);
            if (isGameDraw) {
                gameOverType = Constants.DRAW;
                isGameOver = true;
            }

        }
    }

    private static class Checker {
        private static byte[] currentWinningCombination;

        private static boolean checkForWinner(byte[] currentState, byte playerValue) {
            boolean isWinningCombination = false;
            for (byte[] winningCombination : Constants.WINNING_COMBINATIONS) {
                currentWinningCombination = winningCombination;
                for (byte index : winningCombination) {
                    if (currentState[index] == playerValue || currentState[index] == Constants.NEUTRAL_VALUE) {
                        isWinningCombination = true;
                    } else {
                        isWinningCombination = false;
                        break;
                    }
                }
                if (isWinningCombination) {
                    break;
                }
            }
            return isWinningCombination;
        }

        private static byte[] getWinningLinePoints() {
            return new byte[]{currentWinningCombination[0], currentWinningCombination[2]};
        }

        private static boolean checkForDraw(ParentArea area) {
            return area.checkForDraw();
        }
    }
}
