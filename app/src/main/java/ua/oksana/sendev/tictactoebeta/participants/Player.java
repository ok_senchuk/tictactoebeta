package ua.oksana.sendev.tictactoebeta.participants;


import android.graphics.Bitmap;

public interface Player {
    String getName();
    byte getValue();

}
