package ua.oksana.sendev.tictactoebeta.parts;

import java.util.ArrayList;

/**
 * Created by Oksana on 26.11.2014.
 */
public abstract class Area {

    public <T extends ChildArea> ArrayList<T> initializeField() {
        ArrayList<T> parentField = new ArrayList<T>();
        for (byte i = 0; i < 9; i++) {
            T childField = null;
            try {
                childField = (T) createInstance(i);
            } catch (ClassCastException ex) {
                ex.printStackTrace();
            }
            parentField.add(childField);
        }
        return parentField;
    }

    protected abstract ChildArea createInstance(byte id);
}
