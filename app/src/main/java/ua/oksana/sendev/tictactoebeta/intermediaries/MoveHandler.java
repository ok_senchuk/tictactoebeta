package ua.oksana.sendev.tictactoebeta.intermediaries;

import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.constants.WinningCombination;
import ua.oksana.sendev.tictactoebeta.fragments.ICommunicator;
import ua.oksana.sendev.tictactoebeta.graphics.GameCanvas;
import ua.oksana.sendev.tictactoebeta.graphics.IRefresherInfo;
import ua.oksana.sendev.tictactoebeta.logic.Game;
import ua.oksana.sendev.tictactoebeta.participants.Cross;
import ua.oksana.sendev.tictactoebeta.participants.Player;
import ua.oksana.sendev.tictactoebeta.participants.Zero;

/**
 * Created by Oksana on 08.12.2014.
 */
public class MoveHandler implements IMoveListener {
    private Player cross, zero;
    private GameCanvas gameCanvas;
    private IRefresherInfo refresherInfo;
    private Game game;
    private Player currentPlayer;
    private boolean isGameOver;
    private ArrayList<Character> markMovesInstruction, markMarkersInstruction;
    private byte permittedMiniFields;
    private ICommunicator c;

    public MoveHandler(GameCanvas gameCanvas, ICommunicator c) {
        this.c = c;
        new WinningCombination();
        this.gameCanvas = gameCanvas;
        game = new Game(this);
        gameCanvas.setListener(this);
        initializePlayers();
    }
    @Override
    public void setRefresherInfo(IRefresherInfo refresherInfo) {
        this.refresherInfo = refresherInfo;
    }

    private void initializePlayers() {
        cross = new Cross();
        zero = new Zero();
        currentPlayer = cross;
    }

    @Override
    public void changePlayer() {
        if (currentPlayer == cross) {
            currentPlayer = zero;
        } else if (currentPlayer == zero) {
            currentPlayer = cross;
        }
        refresherInfo.refresh(currentPlayer);
    }


    @Override
    public void makeMove() {
        if (game.set(currentPlayer, gameCanvas.getMiniFieldId(), gameCanvas.getCellId())) {
            boolean isSuccessfulMove = game.start();
            if (isSuccessfulMove) {
                gameCanvas.refresh(markMovesInstruction, markMarkersInstruction, permittedMiniFields);
            }
            if (!isGameOver & isSuccessfulMove) {
                changePlayer();
            }
        }
    }


    @Override
    public void overTheGame(char type, byte[] linePoints, boolean winnerIsNotCurrentPlayer) {
        isGameOver = true;
        if (winnerIsNotCurrentPlayer) {
            changePlayer();
        }
        gameCanvas.refresh(type, currentPlayer, linePoints);
        refresherInfo.refresh(type, currentPlayer);
    }

    @Override
    public void markField(ArrayList<Character> markMovesInstruction, ArrayList<Character> markMarkersInstruction, byte permittedMiniFields) {
        this.markMovesInstruction = markMovesInstruction;
        this.markMarkersInstruction = markMarkersInstruction;
        this.permittedMiniFields = permittedMiniFields;
    }


    @Override
    public void undo() {
        if (game.undoLastMove()) {
            if (isGameOver) {
                isGameOver = false;
                refresherInfo.refresh(currentPlayer);
            } else {
                changePlayer();
            }
            gameCanvas.refresh(markMovesInstruction, markMarkersInstruction, permittedMiniFields, true);
            if (!game.isUndo()) {
                c.disableButtons();
            }
        }
    }

    @Override
    public void enableUndoButton() {
        c.enableButtons();
    }

}
