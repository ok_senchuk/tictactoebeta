package ua.oksana.sendev.tictactoebeta.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ua.oksana.sendev.tictactoebeta.R;

/**
 * Created by Oksana on 18.12.2014.
 */
public class GameRulesActivity extends Activity {
    private TextView t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_rules);
        t = (TextView) findViewById(R.id.rulesTV);
        t.setText(R.string.rules);
    }

    public void backToStartActivity(View v) {
        finish();
    }
}
