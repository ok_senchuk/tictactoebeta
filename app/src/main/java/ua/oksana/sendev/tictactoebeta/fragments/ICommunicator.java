package ua.oksana.sendev.tictactoebeta.fragments;

import ua.oksana.sendev.tictactoebeta.graphics.IRefresherInfo;

/**
 * Created by Oksana on 17.12.2014.
 */
public interface ICommunicator {
    void respond();

    void respond(IRefresherInfo refresherInfo);

    void respond(boolean created);

    void enableButtons();

    void disableButtons();

    void startNewGame();

}
