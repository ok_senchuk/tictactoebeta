package ua.oksana.sendev.tictactoebeta.graphics;

import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.participants.Player;

/**
 * Created by Oksana on 08.12.2014.
 */
public interface IRefresherGame {
    void refresh(ArrayList<Character> markMovesInstruction, ArrayList<Character> markMarkersInstruction, byte permittedMiniFields);

    void refresh(ArrayList<Character> markMovesInstruction, ArrayList<Character> markMarkersInstruction, byte permittedMiniFields, boolean undoFlag);

    void refresh(char type, Player currentPlayer, byte[] linePoints);

}
