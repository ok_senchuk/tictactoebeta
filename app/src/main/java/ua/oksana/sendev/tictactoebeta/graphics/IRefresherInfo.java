package ua.oksana.sendev.tictactoebeta.graphics;

import ua.oksana.sendev.tictactoebeta.participants.Player;

/**
 * Created by Oksana on 17.12.2014.
 */
public interface IRefresherInfo {
    void refresh(Player currentPlayer);

    void refresh(char type, Player currentPlayer);
}
