package ua.oksana.sendev.tictactoebeta.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.constants.Constants;
import ua.oksana.sendev.tictactoebeta.constants.GameBitmaps;
import ua.oksana.sendev.tictactoebeta.intermediaries.IMoveListener;
import ua.oksana.sendev.tictactoebeta.participants.Player;

/**
 * Created by Oksana on 17.12.2014.
 */
public class InfoCanvas extends View implements IRefresherInfo {
    private char info = Constants.CROSS_TURN;
    boolean gameOver;
    private int fieldSideSize=GameBitmaps.getFieldSideSize();


    public InfoCanvas(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawInfo(canvas);
    }

    private void drawInfo(Canvas canvas) {
        canvas.drawBitmap(GameBitmaps.getBitmap(info), 0, 0, null);
    }
    @Override
    public void refresh(Player currentPlayer) {
        if (currentPlayer != null) {
            if (currentPlayer.getName().equals(Constants.CROSS_NAME)) {
                info = Constants.CROSS_TURN;
            } else if (currentPlayer.getName().equals(Constants.ZERO_NAME)) {
                info = Constants.ZERO_TURN;
            }
            invalidate();
        }
    }
    @Override
    public void refresh(char type, Player currentPlayer) {
        if (type == Constants.WIN) {
            if (currentPlayer.getName().equals(Constants.CROSS_NAME)) {
                info = Constants.GAME_OVER_CROSS_WINS;
            } else if (currentPlayer.getName().equals(Constants.ZERO_NAME)) {
                info = Constants.GAME_OVER_ZERO_WINS;
            }
        } else if (type == Constants.DRAW) {
            info = Constants.GAME_OVER_DRAW;
        }
        gameOver = true;
        invalidate();
    }
}
