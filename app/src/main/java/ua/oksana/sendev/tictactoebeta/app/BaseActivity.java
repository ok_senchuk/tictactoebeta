package ua.oksana.sendev.tictactoebeta.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import ua.oksana.sendev.tictactoebeta.factories.FragmentFactory;
import ua.oksana.sendev.tictactoebeta.tools.L;

/**
 * Created by Oksana on 08.12.2014.
 */
public class BaseActivity extends Activity {
    protected FragmentManager manager;
    protected FragmentTransaction transaction;
    private Fragment top;
    private Fragment center;
    private Fragment bottom;

    public void setFragments(String topType, String centerType, String bottomType) {
        manager = getFragmentManager();
        transaction = manager.beginTransaction();
        top = FragmentFactory.getFragment(topType);
        center = FragmentFactory.getFragment(centerType);
        bottom = FragmentFactory.getFragment(bottomType);
        transaction.add(FragmentFactory.getContainer(topType), top, topType);
        transaction.add(FragmentFactory.getContainer(centerType), center, centerType);
        transaction.add(FragmentFactory.getContainer(bottomType), bottom, bottomType);
        transaction.commit();
        L.log("setting fragments");
    }

    public Fragment getTopFragment() {
        L.log("getting top fragment");
        return this.top;
    }

    public Fragment getCenterFragment() {
        L.log("getting center fragment");
        return this.center;
    }

    public Fragment getBottomFragment() {
        L.log("getting bottom fragment");
        return this.bottom;
    }

    public Fragment refresh(String type) {
        Fragment newFragment = FragmentFactory.getFragment(type);
        manager.beginTransaction().replace(FragmentFactory.getContainer(type), newFragment).commit();
        L.log("refresh(String type)");
        L.log("fragment type: "+ type);
        return newFragment;
    }
}
