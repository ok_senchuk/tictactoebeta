package ua.oksana.sendev.tictactoebeta.participants;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import ua.oksana.sendev.tictactoebeta.R;
import ua.oksana.sendev.tictactoebeta.constants.Constants;
import ua.oksana.sendev.tictactoebeta.constants.GameBitmaps;


public class Cross implements Player {
   

    @Override
    public String getName() {
        return Constants.CROSS_NAME;
    }

    @Override
    public byte getValue() {
        return Constants.CROSS_VALUE;
    }


}
