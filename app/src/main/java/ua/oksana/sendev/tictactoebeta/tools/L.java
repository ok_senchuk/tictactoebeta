package ua.oksana.sendev.tictactoebeta.tools;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Oksana on 07.12.2014.
 */
public class L {
    private static StringBuilder logs=new StringBuilder();

    public static void log(String info) {
        Log.i("INFO", info);
        logs.append(info);
        logs.append("\n");
    }

    public static void writeLogsToFile() {
        if (logs != null) {
            String info = logs.toString();
            FileOutputStream output = null;
            OutputStreamWriter outputStreamWriter = null;
            BufferedWriter buff = null;
            try {
                output = new FileOutputStream("output.txt");
                outputStreamWriter = new OutputStreamWriter(output);
                buff = new BufferedWriter(outputStreamWriter);
                buff.write(info);

            } catch (FileNotFoundException ex) {
                System.out.println("File not found. Exception: " + ex);

            } catch (IOException ex) {
                System.out.println("Problem with writing to file. Exception: " + ex);
            } finally {
                try {
                    if (output != null) {
                        assert buff != null;
                        buff.flush();
                        buff.close();
                        outputStreamWriter.close();
                        output.close();
                    }
                } catch (IOException ex) {
                    System.out.println("Problem with closing stream. Exception: "
                            + ex);
                }
            }
        }

    }
}
