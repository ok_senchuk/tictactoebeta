package ua.oksana.sendev.tictactoebeta.logic;

import ua.oksana.sendev.tictactoebeta.participants.Player;

/**
 * Created by Oksana on 08.12.2014.
 */
public interface IGameController {
    boolean set(Player player, byte miniFieldId, byte cellId);
    boolean start();
    boolean undoLastMove();
    boolean isUndo();
}
