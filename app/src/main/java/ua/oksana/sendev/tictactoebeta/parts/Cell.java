package ua.oksana.sendev.tictactoebeta.parts;

import ua.oksana.sendev.tictactoebeta.constants.Constants;

/**
 * Created by Oksana on 17.11.2014.
 */
public class Cell implements ChildArea {
    private final byte id;
    private boolean isOccupied = false;
    private byte value = Constants.EMPTY;

    public Cell(byte id) {
        this.id = id;
    }

    @Override
    public byte getId() {
        return this.id;
    }

    @Override
    public byte getValue() {
        return this.value;
    }

    @Override
    public boolean isOccupied() {
        return this.isOccupied;
    }

    @Override
    public void occupy() {
        this.isOccupied = true;
    }

    @Override
    public void unOccupy() {
        isOccupied=false;
        setValue(Constants.EMPTY);
    }

    @Override
    public void setValue(byte value) {
        this.value = value;
    }
}
