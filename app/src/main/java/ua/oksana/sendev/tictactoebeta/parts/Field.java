package ua.oksana.sendev.tictactoebeta.parts;


import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.constants.Constants;


/**
 * Created by Oksana on 17.11.2014.
 */
public class Field extends Area implements ParentArea, IMarker {

    private ArrayList<MiniField> field;
    private byte[] currentState = {0, 0, 0, 0, 0, 0, 0, 0, 0};


    public Field() {
        field = initializeField();
    }

    @Override
    public byte[] getCurrentState() {
        return this.currentState;
    }

    @Override
    public ChildArea getTarget(byte targetId) {
        return field.get(targetId);
    }

    @Override
    public void refresh(ChildArea target) {
        byte index = target.getId();
        byte value = target.getValue();
        currentState[index] = value;
    }

    @Override
    public boolean checkForDraw() {
        boolean allOccupied = true;
        for (byte state : currentState) {
            if (state == 0) {
                allOccupied = false;
                break;
            }
        }
        return allOccupied;
    }

    @Override
    public ChildArea createInstance(byte id) {
        return new MiniField(id);
    }

    @Override
    public ArrayList<Character> getMarkMarkersInstruction(byte toMark) {
        ArrayList<Character> markInstruction = new ArrayList<Character>();
        for (byte i = 0; i < 9; i++) {
            MiniField miniField = field.get(i);
            if (miniField.isOccupied()) {
                if (miniField.getValue() == Constants.CROSS_VALUE) {
                    markInstruction.add(Constants.MARK_LARGE_CROSS);
                } else if (miniField.getValue() == Constants.ZERO_VALUE) {
                    markInstruction.add(Constants.MARK_LARGE_ZERO);
                } else {
                    markInstruction.add(Constants.MARK_DRAW);
                }
            } else {
                if (miniField.getId() == toMark || toMark == Constants.PERMITTED_ALL) {
                    markInstruction.add(Constants.MARK_NEXT_MOVE);
                } else {
                    markInstruction.add(Constants.NO_MARK);
                }
            }
        }
        return markInstruction;
    }

    @Override
    public ArrayList<Character> getMarkMovesInstruction() {
        ArrayList<Character> markInstruction = new ArrayList<Character>();
        byte m = 0;
        for (byte total = 0; total < 3; total++) {
            byte k = 0;
            for (byte a = 0; a < 3; a++) {
                for (byte i = m; i < m + 3; i++) {
                    MiniField miniField = field.get(i);
                    for (byte j = k; j < k + 3; j++) {
                        Cell temp = (Cell) miniField.getTarget(j);
                        if (temp.isOccupied()) {
                            if (temp.getValue() == Constants.CROSS_VALUE) {
                                markInstruction.add(Constants.MARK_CROSS);
                            } else {
                                markInstruction.add(Constants.MARK_ZERO);
                            }
                        } else {
                            markInstruction.add(Constants.NO_MARK);
                        }
                    }
                }
                k += 3;
            }
            m += 3;
        }
        return markInstruction;
    }
}
