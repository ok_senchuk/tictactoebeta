package ua.oksana.sendev.tictactoebeta.parts;

/**
 * Created by Oksana on 17.11.2014.
 */
public interface ParentArea {
    byte[] getCurrentState();

    ChildArea getTarget(byte targetId);

    void refresh(ChildArea target);

    boolean checkForDraw();

}
