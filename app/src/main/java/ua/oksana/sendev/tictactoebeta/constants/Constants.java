package ua.oksana.sendev.tictactoebeta.constants;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by Oksana on 05.12.2014.
 */
public class Constants {
    public static final String CROSS_NAME = "Cross";
    public static final String ZERO_NAME = "Zero";
    public static final byte CROSS_VALUE = 1;
    public static final byte ZERO_VALUE = 2;
    public static final byte NEUTRAL_VALUE = 3;
    public static final byte EMPTY = 0;
    public static final byte PERMITTED_ALL = 9;
    public static ArrayList<byte[]> WINNING_COMBINATIONS;
    public static final char MARK_CROSS = 'a';
    public static final char MARK_ZERO = 'b';
    public static final char MARK_LARGE_CROSS = 'c';
    public static final char MARK_LARGE_ZERO = 'd';
    public static final char MARK_NEXT_MOVE = 'e';
    public static final char NO_MARK = 'f';
    public static final char MARK_DRAW = 'g';
    public static final char WIN = 'h';
    public static final char DRAW = 'i';
    public static final char BACKGROUND = 'j';
    public static final char CROSS_TURN = 'l';
    public static final char ZERO_TURN = 'm';
    public static final char GAME_OVER_CROSS_WINS = 'n';
    public static final char GAME_OVER_ZERO_WINS = 'o';
    public static final char GAME_OVER_DRAW = 'p';
    public static final char GAME_OVER_ON_FIELD = 'q';
    public static final char GAME_OVER_BACKGROUND_1 = 'r';
    public static final char GAME_OVER_BACKGROUND_2 = 's';
}
