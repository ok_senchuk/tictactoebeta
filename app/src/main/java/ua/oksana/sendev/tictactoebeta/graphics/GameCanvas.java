package ua.oksana.sendev.tictactoebeta.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;


import ua.oksana.sendev.tictactoebeta.constants.Constants;
import ua.oksana.sendev.tictactoebeta.constants.GameBitmaps;
import ua.oksana.sendev.tictactoebeta.intermediaries.IMoveListener;
import ua.oksana.sendev.tictactoebeta.participants.Player;
import ua.oksana.sendev.tictactoebeta.tools.L;

/**
 * Created by Oksana on 08.12.2014.
 */
public class GameCanvas extends View implements IRefresherGame {
    private Workroom w;
    private IMoveListener moveListener;
    private byte permittedMiniFields = Constants.PERMITTED_ALL;
    private ArrayList<Character> markers, moves;
    private char info = Constants.CROSS_TURN;
    private Paint cellDivider, miniFieldDivider, noMarkMarker, nextMarker, zeroMarker, crossMarker, gameOverMarker, lineMarker;
    private int fieldSideSize;
    private int miniFieldSideSize;
    private int cellSideSize;
    private int x, y;
    private byte cellId, miniFieldId;
    private float[][] lineCoordinates;
    boolean gameOver, drawLine, isCrossWinner;


    public GameCanvas(Context context) {
        super(context);
        w = new Workroom();
        w.creator.createArrays();
        w.creator.createPainters();
    }


    public void setListener(IMoveListener moveListener) {
        this.moveListener = moveListener;
    }


    private class Workroom {
        private Creator creator;
        private Calibrator calibrator;
        private Drawer drawer;
        private Calculator calculator;

        private Workroom() {
            creator = new Creator();
            calibrator = new Calibrator();
            drawer = new Drawer();
            calculator = new Calculator();
        }

        private class Creator {
            private void createArrays() {
                moves = new ArrayList<>();
                markers = new ArrayList<>();
                for (byte i = 0; i < 9; i++) {
                    markers.add(Constants.MARK_NEXT_MOVE);
                }
            }

            private void createPainters() {
                cellDivider = new Paint();
                cellDivider.setColor(Color.BLACK);

                miniFieldDivider = new Paint();
                miniFieldDivider.setColor(Color.BLACK);
                miniFieldDivider.setStrokeWidth(3);


                lineMarker = new Paint();
                lineMarker.setColor(Color.RED);
                lineMarker.setStrokeWidth(5);

                noMarkMarker = new Paint();
                noMarkMarker.setAlpha(0);

                nextMarker = new Paint();
                nextMarker.setAlpha(50);

                zeroMarker = new Paint();
                zeroMarker.setColor(Color.parseColor("#FF9999"));

                crossMarker = new Paint();
                crossMarker.setColor(Color.parseColor("#33FF33"));

                gameOverMarker = new Paint();
                gameOverMarker.setAlpha(100);
            }
        }

        private class Calibrator {
            private void setParameters() {
                fieldSideSize = GameBitmaps.getFieldSideSize();
                miniFieldSideSize = fieldSideSize / 3;
                cellSideSize = fieldSideSize / 9;
            }

        }

        private class Drawer {
            private void drawMarkers(Canvas canvas) {
                for (int i = 0; i < markers.size(); i++) {
                    char type = markers.get(i);
                    Paint marker = setMarker(type);
                    switch (i) {
                        case 0:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), 0, 0, marker);
                            break;
                        case 1:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), miniFieldSideSize, 0, marker);
                            break;
                        case 2:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), 2 * miniFieldSideSize, 0, marker);
                            break;
                        case 3:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), 0, miniFieldSideSize, marker);
                            break;
                        case 4:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), miniFieldSideSize, miniFieldSideSize, marker);
                            break;
                        case 5:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), 2 * miniFieldSideSize, miniFieldSideSize, marker);
                            break;
                        case 6:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), 0, 2 * miniFieldSideSize, marker);
                            break;
                        case 7:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), miniFieldSideSize, 2 * miniFieldSideSize, marker);
                            break;
                        case 8:
                            canvas.drawBitmap(GameBitmaps.getBitmap(type), 2 * miniFieldSideSize, 2 * miniFieldSideSize, marker);
                            break;
                    }

                }
            }

            private Paint setMarker(char type) {
                Paint marker = null;
                if (type == Constants.MARK_LARGE_CROSS) {
                    marker = crossMarker;
                } else if (type == Constants.MARK_LARGE_ZERO) {
                    marker = zeroMarker;
                }
                if (type == Constants.MARK_NEXT_MOVE) {
                    marker = nextMarker;
                }
                if (type == Constants.NO_MARK) {
                    marker = noMarkMarker;
                }
                return marker;
            }

            private void drawMoves(Canvas canvas) {
                for (int i = 0; i < moves.size(); i++) {
                    char type = moves.get(i);
                    if (i <= 8) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), i * cellSideSize, 0, null);
                    } else if (i >= 9 && i <= 17) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 9) * cellSideSize, cellSideSize, null);
                    } else if (i >= 18 && i <= 26) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 18) * cellSideSize, 2 * cellSideSize, null);
                    } else if (i >= 27 && i <= 35) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 27) * cellSideSize, 3 * cellSideSize, null);
                    } else if (i >= 36 && i <= 44) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 36) * cellSideSize, 4 * cellSideSize, null);
                    } else if (i >= 45 && i <= 53) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 45) * cellSideSize, 5 * cellSideSize, null);
                    } else if (i >= 54 && i <= 62) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 54) * cellSideSize, 6 * cellSideSize, null);
                    } else if (i >= 63 && i <= 71) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 63) * cellSideSize, 7 * cellSideSize, null);
                    } else if (i >= 72 && i <= 80) {
                        canvas.drawBitmap(GameBitmaps.getBitmap(type), (i - 72) * cellSideSize, 8 * cellSideSize, null);
                    }
                }
            }

            private void drawGrid(Canvas canvas) {
                for (byte i = 0; i < 10; i++) {
                    Paint currentDivider = cellDivider;
                    if (i == 0 || i == 3 || i == 6 || i == 9) {
                        currentDivider = miniFieldDivider;
                    }
                    canvas.drawLine(0, i * cellSideSize, fieldSideSize, i * cellSideSize, currentDivider);
                    canvas.drawLine(i * cellSideSize, 0, i * cellSideSize, fieldSideSize, currentDivider);
                }
            }

            private void drawInfo(Canvas canvas) {
                if (isCrossWinner) {
                    canvas.drawBitmap(GameBitmaps.getBitmap(Constants.GAME_OVER_BACKGROUND_1), 0, 0, gameOverMarker);
                    isCrossWinner = false;
                } else {
                    canvas.drawBitmap(GameBitmaps.getBitmap(Constants.GAME_OVER_BACKGROUND_2), 0, 0, gameOverMarker);
                }
                canvas.drawBitmap(GameBitmaps.getBitmap(Constants.GAME_OVER_ON_FIELD), 0, 0, null);
            }
        }

        private class Calculator {
            private void calculateIdsFromCoordinates() {
                L.log("GameFieldCanvas calculateIdsFromCoordinates()");
                calculateCellId();
                calculateMiniFieldId();
            }

            private void calculateCellId() {
                L.log("GameFieldCanvas calculateCellId()");
                if (y == 0 || y == 3 || y == 6) {
                    if (x == 0 || x == 3 || x == 6) {
                        cellId = 0;
                    } else if (x == 1 || x == 4 || x == 7) {
                        cellId = 1;
                    } else if (x == 2 || x == 5 || x == 8) {
                        cellId = 2;
                    } else {
                        L.log("Some error in calculateCellId()");
                    }
                }

                if (y == 1 || y == 4 || y == 7) {
                    if (x == 0 || x == 3 || x == 6) {
                        cellId = 3;
                    } else if (x == 1 || x == 4 || x == 7) {
                        cellId = 4;
                    } else if (x == 2 || x == 5 || x == 8) {
                        cellId = 5;
                    } else {
                        L.log("Some error in calculateCellId()");
                    }
                }

                if (y == 2 || y == 5 || y == 8) {
                    if (x == 0 || x == 3 || x == 6) {
                        cellId = 6;
                    } else if (x == 1 || x == 4 || x == 7) {
                        cellId = 7;
                    } else if (x == 2 || x == 5 || x == 8) {
                        cellId = 8;
                    } else {
                        L.log("Some error in calculateCellId()");
                    }

                }
                L.log("cellId: " + cellId);
            }

            private void calculateMiniFieldId() {
                L.log("GameFieldCanvas calculateMiniFieldId()");
                if (x < 3) {
                    if (y < 3) {
                        miniFieldId = 0;
                    } else if (y > 2 && y < 6) {
                        miniFieldId = 3;
                    } else {
                        miniFieldId = 6;
                    }
                } else if (x > 2 && x < 6) {
                    if (y < 3) {
                        miniFieldId = 1;
                    } else if (y > 2 && y < 6) {
                        miniFieldId = 4;
                    } else {
                        miniFieldId = 7;
                    }
                } else if (x > 5 && x < 9) {
                    if (y < 3) {
                        miniFieldId = 2;
                    } else if (y > 2 && y < 6) {
                        miniFieldId = 5;
                    } else {
                        miniFieldId = 8;
                    }
                } else {
                    L.log("Some error in calculateMiniFieldId()");
                }
                L.log("miniFieldId: " + miniFieldId);
            }

            private void calculateVictoryLineCoordinates(byte[] points) {

                byte startLineMiniFieldId = points[0];
                byte endLineMiniFieldId = points[1];

                if (startLineMiniFieldId == 0) {
                    switch (endLineMiniFieldId) {
                        case 2:
                            lineCoordinates = getCoordinates((byte) 1);
                            break;
                        case 6:
                            lineCoordinates = getCoordinates((byte) 2);
                            break;
                        case 8:
                            lineCoordinates = getCoordinates((byte) 3);
                            break;
                    }
                } else if (startLineMiniFieldId == 1) {
                    lineCoordinates = getCoordinates((byte) 4);
                } else if (startLineMiniFieldId == 2) {
                    switch (endLineMiniFieldId) {
                        case 6:
                            lineCoordinates = getCoordinates((byte) 5);
                            break;
                        case 8:
                            lineCoordinates = getCoordinates((byte) 6);
                            break;
                    }
                } else if (startLineMiniFieldId == 3) {
                    lineCoordinates = getCoordinates((byte) 7);
                } else if (startLineMiniFieldId == 6) {
                    lineCoordinates = getCoordinates((byte) 8);
                }
            }


            private float[][] getCoordinates(byte id) {
                float l1 = fieldSideSize / 2;
                float l2 = fieldSideSize / 6;
                float l3 = fieldSideSize / 18;
                float l4 = fieldSideSize - fieldSideSize / 6;
                float l5 = fieldSideSize - fieldSideSize / 18;
                float x1 = 0;
                float y1 = 0;
                float x2 = 0;
                float y2 = 0;
                switch (id) {
                    case 1:
                        x1 = l3;
                        y1 = l2;
                        x2 = l5;
                        y2 = l2;
                        break;
                    case 2:
                        x1 = l2;
                        y1 = l3;
                        x2 = l2;
                        y2 = l5;
                        break;
                    case 3:
                        x1 = l3;
                        y1 = l3;
                        x2 = l5;
                        y2 = l5;
                        break;
                    case 4:
                        x1 = l1;
                        y1 = l3;
                        x2 = l1;
                        y2 = l5;
                        break;
                    case 5:
                        x1 = l5;
                        y1 = l3;
                        x2 = l3;
                        y2 = l5;

                        break;
                    case 6:
                        x1 = l4;
                        y1 = l3;
                        x2 = l4;
                        y2 = l5;
                        break;
                    case 7:
                        x1 = l3;
                        y1 = l1;
                        x2 = l5;
                        y2 = l1;
                        break;
                    case 8:
                        x1 = l3;
                        y1 = l4;
                        x2 = l5;
                        y2 = l4;
                        break;
                }
                return new float[][]{{x1, y1}, {x2, y2}};
            }

        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        w.calibrator.setParameters();
        canvas.drawBitmap(GameBitmaps.getBitmap(Constants.BACKGROUND), 0, 0, null);
        w.drawer.drawMoves(canvas);
        w.drawer.drawMarkers(canvas);
        w.drawer.drawGrid(canvas);
        if (drawLine) {
            canvas.drawLine(lineCoordinates[0][0], lineCoordinates[0][1], lineCoordinates[1][0], lineCoordinates[1][1], lineMarker);
            drawLine = false;
        }
        if (gameOver) {
            w.drawer.drawInfo(canvas);
        }
    }


    @Override
    public void refresh(ArrayList<Character> markMovesInstruction, ArrayList<Character> markMarkersInstruction, byte permittedMiniFields) {
        this.permittedMiniFields = permittedMiniFields;
        this.moves = markMovesInstruction;
        this.markers = markMarkersInstruction;
        invalidate();
    }

    @Override
    public void refresh(ArrayList<Character> markMovesInstruction, ArrayList<Character> markMarkersInstruction, byte permittedMiniFields, boolean undoFlag) {
        if (gameOver) {
            gameOver = false;
            lineCoordinates = null;
        }
        refresh(markMovesInstruction, markMarkersInstruction, permittedMiniFields);
    }


    @Override
    public void refresh(char type, Player currentPlayer, byte[] linePoints) {
        if (type != Constants.DRAW) {
            drawLine = true;
            w.calculator.calculateVictoryLineCoordinates(linePoints);
        }
        gameOver = true;
        if (currentPlayer.getName().equals(Constants.CROSS_NAME)) {
            isCrossWinner = true;
        }
        invalidate();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean toReturn = super.onTouchEvent(event);
        if (!gameOver) {
            x = (int) (event.getX() / cellSideSize);
            y = (int) (event.getY() / cellSideSize);
            if (x < 9 & y < 9) {
                L.log("GameFieldCanvas makeGameMove()");
                w.calculator.calculateIdsFromCoordinates();
                if (miniFieldId == permittedMiniFields || permittedMiniFields == Constants.PERMITTED_ALL) {
                    moveListener.makeMove();
                    moveListener.enableUndoButton();
                }
            }
        }
        return toReturn;
    }

    public byte getMiniFieldId() {
        return this.miniFieldId;
    }

    public byte getCellId() {
        return this.cellId;
    }
}
