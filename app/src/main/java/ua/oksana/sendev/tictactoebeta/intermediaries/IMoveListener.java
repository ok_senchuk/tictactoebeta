package ua.oksana.sendev.tictactoebeta.intermediaries;

import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.graphics.IRefresherInfo;

/**
 * Created by Oksana on 08.12.2014.
 */
public interface IMoveListener {
    void makeMove();

    void overTheGame(char type, byte[] linePoints, boolean winnerIsNotCurrentPlayer);

    void markField(ArrayList<Character> markMovesInstruction, ArrayList<Character> markMarkersInstruction, byte permittedMiniFields);

    void undo();

    void enableUndoButton();

    void changePlayer();

    void setRefresherInfo(IRefresherInfo refresherInfo);
}
