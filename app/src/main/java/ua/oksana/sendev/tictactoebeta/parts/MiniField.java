package ua.oksana.sendev.tictactoebeta.parts;


import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.constants.Constants;


public class MiniField extends Area implements ChildArea, ParentArea {
    private final byte id;
    private boolean isOccupied = false;
    private byte value = Constants.EMPTY;
    private byte[] currentState = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    private ArrayList<Cell> cells;

    public MiniField(byte id) {
        this.id = id;
        cells = initializeField();
    }

    @Override
    public byte getId() {
        return this.id;
    }

    @Override
    public byte getValue() {
        return this.value;
    }

    @Override
    public boolean isOccupied() {
        return this.isOccupied;
    }

    @Override
    public void occupy() {
        this.isOccupied = true;
    }

    @Override
    public void unOccupy() {
        isOccupied=false;
        setValue(Constants.EMPTY);
    }

    @Override
    public void setValue(byte value) {
        this.value = value;
    }


    @Override
    protected ChildArea createInstance(byte id) {
        return new Cell(id);
    }


    @Override
    public byte[] getCurrentState() {
        return this.currentState;
    }

    @Override
    public ChildArea getTarget(byte targetId) {
        return cells.get(targetId);
    }

    @Override
    public void refresh(ChildArea target) {
        byte index = target.getId();
        byte value = target.getValue();
        currentState[index] = value;
    }

    @Override
    public boolean checkForDraw() {
        boolean allOccupied = true;
        for (byte state : currentState) {
            if (state == 0) {
                allOccupied = false;
                break;
            }
        }
        if (allOccupied) {
            setValue(Constants.NEUTRAL_VALUE);
            occupy();
        }
        return allOccupied;
    }

}
