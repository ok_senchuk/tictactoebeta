package ua.oksana.sendev.tictactoebeta.constants;

import java.util.ArrayList;

/**
 * Created by Oksana on 17.11.2014.
 */
public class WinningCombination {
    private static final byte[] C1 = {0, 1, 2};
    private static final byte[] C2 = {3, 4, 5};
    private static final byte[] C3 = {6, 7, 8};
    private static final byte[] C4 = {0, 3, 6};
    private static final byte[] C5 = {1, 4, 7};
    private static final byte[] C6 = {2, 5, 8};
    private static final byte[] C7 = {0, 4, 8};
    private static final byte[] C8 = {2, 4, 6};

    static {
        Constants.WINNING_COMBINATIONS = new ArrayList<>();
        Constants.WINNING_COMBINATIONS.add(C1);
        Constants.WINNING_COMBINATIONS.add(C2);
        Constants.WINNING_COMBINATIONS.add(C3);
        Constants.WINNING_COMBINATIONS.add(C4);
        Constants.WINNING_COMBINATIONS.add(C5);
        Constants.WINNING_COMBINATIONS.add(C6);
        Constants.WINNING_COMBINATIONS.add(C7);
        Constants.WINNING_COMBINATIONS.add(C8);
    }
}
