package ua.oksana.sendev.tictactoebeta.parts;

/**
 * Created by Oksana on 17.11.2014.
 */
public interface ChildArea {
    byte getId();
    byte getValue();
    boolean isOccupied();
    void occupy();
    void unOccupy();
    void setValue(byte value);

}
