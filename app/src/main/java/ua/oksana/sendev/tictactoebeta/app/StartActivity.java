package ua.oksana.sendev.tictactoebeta.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Locale;

import ua.oksana.sendev.tictactoebeta.R;
import ua.oksana.sendev.tictactoebeta.constants.GameBitmaps;


public class StartActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        createBitmaps();
    }

    private void createBitmaps() {
        new GameBitmaps(StartActivity.this).start();
    }

    private void setLocal(String lang) {
        Locale locale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        onConfigurationChanged(conf);
    }

    public void startNewGame(View v) {
        Intent i = new Intent(StartActivity.this, GameFieldActivity.class);
        startActivity(i);
    }

    public void showRules(View v) {
        Intent i = new Intent(StartActivity.this, GameRulesActivity.class);
        startActivity(i);
    }

    public void changeLanguageToUk(View v) {
        setLocal("uk");
    }

    public void showInfo(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
        builder.setTitle(getString(R.string.about_title))
                .setMessage(getString(R.string.about_info))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void changeLanguageToEn(View v) {
        setLocal("en");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_start);
        createBitmaps();
    }
}
