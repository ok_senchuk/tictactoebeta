package ua.oksana.sendev.tictactoebeta.app;

import android.os.Bundle;

import ua.oksana.sendev.tictactoebeta.R;
import ua.oksana.sendev.tictactoebeta.constants.FragmentTypes;
import ua.oksana.sendev.tictactoebeta.fragments.GameFieldFragment;
import ua.oksana.sendev.tictactoebeta.fragments.ICommunicator;
import ua.oksana.sendev.tictactoebeta.fragments.ControlFragment;
import ua.oksana.sendev.tictactoebeta.fragments.MoveInfoFragment;
import ua.oksana.sendev.tictactoebeta.graphics.IRefresherInfo;
import ua.oksana.sendev.tictactoebeta.tools.L;

/**
 * Created by Oksana on 08.12.2014.
 */
public class GameFieldActivity extends BaseActivity implements ICommunicator {
    private ControlFragment controlFragment;
    private GameFieldFragment gameFieldFragment;
    private MoveInfoFragment moveInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_field);
        constructAppearance();
        L.log("onCreate Activity");
    }

    private void constructAppearance() {
        setFragments(FragmentTypes.GAME_TOP, FragmentTypes.GAME_CENTER, FragmentTypes.GAME_BOTTOM);
        controlFragment = (ControlFragment) getBottomFragment();
        gameFieldFragment = (GameFieldFragment) getCenterFragment();
        moveInfoFragment = (MoveInfoFragment) getTopFragment();

    }


    @Override
    public void respond() {
        gameFieldFragment.undoMove();
    }

    @Override
    public void respond(IRefresherInfo refresherInfo) {
        gameFieldFragment.setRefresher(refresherInfo);
    }

    @Override
    public void respond(boolean created) {
        moveInfoFragment.getRefresher();
    }


    @Override
    public void enableButtons() {
        controlFragment.enableButtons();
    }

    @Override
    public void disableButtons() {
        controlFragment.disableButtons();
    }

    @Override
    public void startNewGame() {
        moveInfoFragment = (MoveInfoFragment) refresh(FragmentTypes.GAME_TOP);
        gameFieldFragment = (GameFieldFragment) refresh(FragmentTypes.GAME_CENTER);
        controlFragment.disableButtons();
    }
}
