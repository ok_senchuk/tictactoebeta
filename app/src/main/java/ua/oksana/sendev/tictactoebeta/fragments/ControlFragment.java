package ua.oksana.sendev.tictactoebeta.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ua.oksana.sendev.tictactoebeta.R;

/**
 * Created by Oksana on 08.12.2014.
 */
public class ControlFragment extends Fragment {
    private ICommunicator c;
    private View v;
    private Button undoBtn, newGameBtn;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.c = (ICommunicator) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_control, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        undoBtn = (Button) v.findViewById(R.id.undoBtn);
        undoBtn.setEnabled(false);
        undoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c.respond();
            }
        });
        newGameBtn = (Button) v.findViewById(R.id.restartBtn);
        newGameBtn.setEnabled(false);
        newGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              showDialog();
            }
        });
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder((Context)c);
        builder.setMessage(getString(R.string.dialog_question))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.dialog_answer_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                      c.startNewGame();
                    }
                })
                .setNegativeButton(getString(R.string.dialog_answer_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void enableButtons() {
        undoBtn.setEnabled(true);
        newGameBtn.setEnabled(true);
    }

    public void disableButtons() {
        undoBtn.setEnabled(false);
        newGameBtn.setEnabled(false);
    }


}
