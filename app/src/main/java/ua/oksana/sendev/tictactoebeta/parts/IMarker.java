package ua.oksana.sendev.tictactoebeta.parts;

import java.util.ArrayList;

/**
 * Created by Oksana on 08.12.2014.
 */
public interface IMarker {
    ArrayList<Character> getMarkMarkersInstruction(byte toMark);
    ArrayList<Character> getMarkMovesInstruction();
}
