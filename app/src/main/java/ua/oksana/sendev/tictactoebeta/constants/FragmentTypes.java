package ua.oksana.sendev.tictactoebeta.constants;

/**
 * Created by Oksana on 08.12.2014.
 */
public class FragmentTypes {
    public static final String GAME_TOP ="game_top";
    public static final String GAME_CENTER ="game_center";
    public static final String GAME_BOTTOM ="game_bottom";
}
