package ua.oksana.sendev.tictactoebeta.logic;

import java.util.ArrayList;

import ua.oksana.sendev.tictactoebeta.participants.Player;

/**
 * Created by Oksana on 12.12.2014.
 */
public class MoveSaver {
    private ArrayList<Byte> savedPermittedMiniField;
    private ArrayList<Byte> savedCellIds;
    private ArrayList<Byte> savedMiniFieldIds;



    public MoveSaver() {
        savedCellIds = new ArrayList<>();
        savedPermittedMiniField = new ArrayList<>();
        savedMiniFieldIds = new ArrayList<>();

    }

    public void save(int moveCounter,byte permittedMiniField, byte miniFieldId, byte cellId) {
        savedPermittedMiniField.add(moveCounter-1, permittedMiniField);
        savedMiniFieldIds.add(moveCounter-1, miniFieldId);
        savedCellIds.add(moveCounter-1, cellId);


    }

    public void cleanUp(int moveCounter) {
        savedPermittedMiniField.remove(moveCounter - 1);
        savedMiniFieldIds.remove(moveCounter - 1);
        savedCellIds.remove(moveCounter - 1);
    }

    public byte getSavedPermittedMiniField(int moveCounter){
        return savedPermittedMiniField.get(moveCounter - 1);
    }
    public byte getSavedMiniFieldId(int moveCounter){
        return savedMiniFieldIds.get(moveCounter - 1);
    }
    public byte getSavedCellId(int moveCounter){
        return savedCellIds.get(moveCounter - 1);
    }
}
