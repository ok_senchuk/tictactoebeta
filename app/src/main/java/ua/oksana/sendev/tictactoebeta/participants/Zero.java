package ua.oksana.sendev.tictactoebeta.participants;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import ua.oksana.sendev.tictactoebeta.R;
import ua.oksana.sendev.tictactoebeta.constants.Constants;
import ua.oksana.sendev.tictactoebeta.constants.GameBitmaps;


public class Zero implements Player {

    @Override
    public String getName() {
        return Constants.ZERO_NAME;
    }

    @Override
    public byte getValue() {
        return Constants.ZERO_VALUE;
    }
}
