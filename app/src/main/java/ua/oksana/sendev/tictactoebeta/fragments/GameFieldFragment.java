package ua.oksana.sendev.tictactoebeta.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import ua.oksana.sendev.tictactoebeta.graphics.GameCanvas;
import ua.oksana.sendev.tictactoebeta.graphics.IRefresherInfo;
import ua.oksana.sendev.tictactoebeta.intermediaries.MoveHandler;
import ua.oksana.sendev.tictactoebeta.tools.L;

/**
 * Created by Oksana on 08.12.2014.
 */
public class GameFieldFragment extends Fragment {
    private GameCanvas canvas;
    private ICommunicator c;
    private MoveHandler moveHandler;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        canvas = new GameCanvas(activity);
        moveHandler = new MoveHandler(canvas, (ICommunicator) activity);
        c= (ICommunicator) activity;
        c.respond(true);
        L.log("onAttach");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.log("onCreateView");
        return canvas;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        L.log("onActivityCreated");
    }

    public void undoMove() {
        moveHandler.undo();
    }

    public void setRefresher(IRefresherInfo refresher) {
        moveHandler.setRefresherInfo(refresher);
    }

}
