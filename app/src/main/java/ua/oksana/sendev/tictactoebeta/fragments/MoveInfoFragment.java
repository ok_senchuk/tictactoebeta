package ua.oksana.sendev.tictactoebeta.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import ua.oksana.sendev.tictactoebeta.R;
import ua.oksana.sendev.tictactoebeta.graphics.IRefresherInfo;
import ua.oksana.sendev.tictactoebeta.graphics.InfoCanvas;

/**
 * Created by Oksana on 17.12.2014.
 */
public class MoveInfoFragment extends Fragment {
    private ICommunicator c;
    private InfoCanvas v;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.c = (ICommunicator) activity;
        v = new InfoCanvas(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void getRefresher(){
        c.respond(v);
    }
}
