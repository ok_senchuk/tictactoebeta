package ua.oksana.sendev.tictactoebeta.factories;

import android.app.Fragment;

import ua.oksana.sendev.tictactoebeta.R;
import ua.oksana.sendev.tictactoebeta.constants.FragmentTypes;
import ua.oksana.sendev.tictactoebeta.fragments.GameFieldFragment;
import ua.oksana.sendev.tictactoebeta.fragments.ControlFragment;
import ua.oksana.sendev.tictactoebeta.fragments.MoveInfoFragment;

/**
 * Created by Oksana on 08.12.2014.
 */
public class FragmentFactory {
    private static Fragment fragment;
    private static int container;

    public static Fragment getFragment(String type) {
        if (type.equals(FragmentTypes.GAME_TOP)) {
            fragment = new MoveInfoFragment();
        } else if (type.equals(FragmentTypes.GAME_CENTER)) {
            fragment = new GameFieldFragment();
        }else if (type.equals(FragmentTypes.GAME_BOTTOM)) {
            fragment = new ControlFragment();
        }
        return fragment;
    }

    public static int getContainer(String type) {
        if (type.equals(FragmentTypes.GAME_TOP)) {
            container = R.id.gameActivityTopContainerLL;
        } else if (type.equals(FragmentTypes.GAME_CENTER)) {
            container = R.id.gameActivityCenterContainerLL;
        }else if (type.equals(FragmentTypes.GAME_BOTTOM)) {
            container = R.id.gameActivityBottomContainerLL;
        }
        return container;
    }
}
