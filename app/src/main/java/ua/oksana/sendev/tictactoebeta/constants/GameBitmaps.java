package ua.oksana.sendev.tictactoebeta.constants;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import ua.oksana.sendev.tictactoebeta.R;

/**
 * Created by Oksana on 15.12.2014.
 */
public class GameBitmaps extends Thread {
    private int residualSideSize;
    private static int fieldSideSize;
    private int miniFieldSideSize;
    private int cellSideSize;
    private Context c;

    private static Bitmap next, scaledBackground, scaledCross, scaledZero, scaledLargeCross, scaledLargeZero, scaledCrossTurn, scaledZeroTurn, scaledDrawOnMiniField, scaledGameOverCrossWins, scaledGameOverDraw, scaledGameOverOnField, scaledGameOverZeroWins, noMark, gameOverOnFieldBcg1, gameOverOnFieldBcg2;

    public GameBitmaps(Context c) {
        this.c = c;
    }

    @Override
    public void run() {
        super.run();
        calculateParameters();
        createBitmaps();
    }

    private void createBitmaps() {
        next = Bitmap.createBitmap(miniFieldSideSize, miniFieldSideSize, Bitmap.Config.ARGB_8888);
        next.eraseColor(Color.YELLOW);
        noMark = Bitmap.createBitmap(miniFieldSideSize, miniFieldSideSize, Bitmap.Config.ARGB_8888);
        gameOverOnFieldBcg1 = Bitmap.createBitmap(fieldSideSize, fieldSideSize, Bitmap.Config.ARGB_8888);
        gameOverOnFieldBcg1.eraseColor(Color.GREEN);
        gameOverOnFieldBcg2 = Bitmap.createBitmap(fieldSideSize, fieldSideSize, Bitmap.Config.ARGB_8888);
        gameOverOnFieldBcg2.eraseColor(Color.MAGENTA);
        Bitmap background = BitmapFactory.decodeResource(c.getResources(), R.drawable.bcg);
        scaledBackground = Bitmap.createScaledBitmap(background, fieldSideSize, fieldSideSize, true);
        background.recycle();
        Bitmap cross = BitmapFactory.decodeResource(c.getResources(), R.drawable.cross);
        scaledCross = Bitmap.createScaledBitmap(cross, cellSideSize, cellSideSize, true);
        cross.recycle();
        Bitmap zero = BitmapFactory.decodeResource(c.getResources(), R.drawable.zero);
        scaledZero = Bitmap.createScaledBitmap(zero, cellSideSize, cellSideSize, true);
        zero.recycle();
        Bitmap largeCross = BitmapFactory.decodeResource(c.getResources(), R.drawable.large_cross);
        scaledLargeCross = Bitmap.createScaledBitmap(largeCross, miniFieldSideSize, miniFieldSideSize, true);
        largeCross.recycle();
        Bitmap largeZero = BitmapFactory.decodeResource(c.getResources(), R.drawable.large_zero);
        scaledLargeZero = Bitmap.createScaledBitmap(largeZero, miniFieldSideSize, miniFieldSideSize, true);
        largeZero.recycle();
        Bitmap zeroTurn = BitmapFactory.decodeResource(c.getResources(), R.drawable.zero_turn);
        scaledZeroTurn = Bitmap.createScaledBitmap(zeroTurn, fieldSideSize, residualSideSize, true);
        zeroTurn.recycle();
        Bitmap crossTurn = BitmapFactory.decodeResource(c.getResources(), R.drawable.cross_turn);
        scaledCrossTurn = Bitmap.createScaledBitmap(crossTurn, fieldSideSize, residualSideSize, true);
        crossTurn.recycle();
        Bitmap drawOnMiniField = BitmapFactory.decodeResource(c.getResources(), R.drawable.no_winner);
        scaledDrawOnMiniField = Bitmap.createScaledBitmap(drawOnMiniField, miniFieldSideSize, miniFieldSideSize, true);
        drawOnMiniField.recycle();
        Bitmap gameOverCrossWins = BitmapFactory.decodeResource(c.getResources(), R.drawable.game_over_cross_wins);
        scaledGameOverCrossWins = Bitmap.createScaledBitmap(gameOverCrossWins, fieldSideSize, residualSideSize, true);
        gameOverCrossWins.recycle();
        Bitmap gameOverDraw = BitmapFactory.decodeResource(c.getResources(), R.drawable.game_over_draw);
        scaledGameOverDraw = Bitmap.createScaledBitmap(gameOverDraw, fieldSideSize, residualSideSize, true);
        gameOverDraw.recycle();
        Bitmap gameOverOnField = BitmapFactory.decodeResource(c.getResources(), R.drawable.game_over_on_field);
        scaledGameOverOnField = Bitmap.createScaledBitmap(gameOverOnField, fieldSideSize, fieldSideSize, true);
        gameOverOnField.recycle();
        Bitmap gameOverZeroWins = BitmapFactory.decodeResource(c.getResources(), R.drawable.game_over_zero_wins);
        scaledGameOverZeroWins = Bitmap.createScaledBitmap(gameOverZeroWins, fieldSideSize, residualSideSize, true);
        gameOverZeroWins.recycle();
    }

    private void calculateParameters() {
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        fieldSideSize = width;
        miniFieldSideSize = fieldSideSize / 3;
        cellSideSize = miniFieldSideSize / 3;
        residualSideSize = (height - fieldSideSize) / 3;
    }


    public static Bitmap getBitmap(char type) {
        Bitmap temp = null;
        switch (type) {
            case Constants.BACKGROUND:
                temp = scaledBackground;
                break;
            case Constants.MARK_CROSS:
                temp = scaledCross;
                break;
            case Constants.MARK_ZERO:
                temp = scaledZero;
                break;
            case Constants.CROSS_TURN:
                temp = scaledCrossTurn;
                break;
            case Constants.ZERO_TURN:
                temp = scaledZeroTurn;
                break;
            case Constants.GAME_OVER_CROSS_WINS:
                temp = scaledGameOverCrossWins;
                break;
            case Constants.GAME_OVER_ZERO_WINS:
                temp = scaledGameOverZeroWins;
                break;
            case Constants.GAME_OVER_DRAW:
                temp = scaledGameOverDraw;
                break;
            case Constants.GAME_OVER_ON_FIELD:
                temp = scaledGameOverOnField;
                break;
            case Constants.MARK_LARGE_CROSS:
                temp = scaledLargeCross;
                break;
            case Constants.MARK_LARGE_ZERO:
                temp = scaledLargeZero;
                break;
            case Constants.MARK_NEXT_MOVE:
                temp = next;
                break;
            case Constants.NO_MARK:
                temp = noMark;
                break;
            case Constants.MARK_DRAW:
                temp = scaledDrawOnMiniField;
                break;
            case Constants.GAME_OVER_BACKGROUND_1:
                temp = gameOverOnFieldBcg1;
                break;
            case Constants.GAME_OVER_BACKGROUND_2:
                temp = gameOverOnFieldBcg2;
                break;
        }
        return temp;
    }

    public static int getFieldSideSize() {
        return fieldSideSize;
    }
}
